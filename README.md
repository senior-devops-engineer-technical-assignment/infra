# Senior DevOps Engineer Technical Assignment

[![pipeline status](https://gitlab.com/senior-devops-engineer-technical-assignment/infra/badges/main/pipeline.svg)](https://gitlab.com/senior-devops-engineer-technical-assignment/infra/-/commits/main)

- [Senior DevOps Engineer Technical Assignment](#senior-devops-engineer-technical-assignment)
  - [Create Infra and Deploy app using:](#create-infra-and-deploy-app-using)
  - [The infra](#the-infra)
  - [Understanding the infrastructure](#understanding-the-infrastructure)
  - [Gitflow](#gitflow)
  - [How to run locally](#how-to-run-locally)
    - [Prerequisites](#prerequisites)
  - [Security](#security)
  - [App Roadmap](#app-roadmap)
    - [To future](#to-future)

## Create Infra and Deploy app using:
Infra -> Terraform (Kubernetes Cluster)
Cloud provider -> AWS
CI/CD platform -> Gitlab CI/CD
Code Versioning -> Gitlab

## The infra
The infraestructure consist in:
- An EKS Cluster version 1.27 with 4 nodes in the same version
- An [isito](https://istio.io) version 1.20.3 serving the apps as service-mesh
- Three custom namespaces based in the branches


## Understanding the infrastructure

The terraform is configured to use a specific bucket as the infra state. So, to run in a different AWS account, you need to create a new bucket and fill the [backend.tf](backend.tf) infos.

Also, the terraform is using an [official terraform](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest) module to deploy the cluster with less complexity.

After the creation, the common change in the nodes should be to:
  - Upgrade nodes to a new version
    - check the `ami_release_version` in [variables.tf](variables.tf), in case a new upgrade you just need to change the default values to the next version
  - Change the desired nodes
    - In case to scale in the cluster, change the `min_size` and `desired_size` in [eks.tf](eks.tf)

## Gitflow
All the changes needs to be made following the gitflow:
1. Push the new code to stage branch
2. Check if the terraform plan will run correctly
3. Request a MR from stage to main (Needs more than 1 reviewer)
4. Add the new code via MR to main
5. Approve the terraform apply stage


## How to run locally

### Prerequisites
Have installed:
    - terraform
    - Git

1. Clone this repo in your machine
2. Enter in the folder
3. Change the backend.tf file to your bucket
4. Run
```bash
terraform init #Will initialize all the modules and dependencies
terraform plan -out tfplan #Will plan your infra as-is with the new infra code
terraform apply tfplan #Will apply the difference in your infra
```


## Security
This app uses AWS authentication as Gitlab Variables to connect avoiding any sensitive info versioned in the code.
The user to deploy the infra is a dedicated user that works only for this purpose.

## App Roadmap
v0.1 -> Run the infra code locally
v0.2 -> Run the infra code with pipeline
v0.3 -> Run the infra code with pipelines and variables
v0.3.1 -> Bugfix in the pipeline
### To future
`This versions could not be done due the time of exercise`
v0.5 -> Add the terraform fmt, security and checov as new stages in the pipline
v0.6 -> Decrease the permissions of the user only to have the necessary rights